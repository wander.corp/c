﻿
#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Animal
{
public:
	virtual void voice()
	{
		cout << "sound: ";
	}
};

class Dog : public Animal
{
public:
	void voice() override
	{
		cout << "gaf, gaf! ";
	}
};

class Cat : public Animal
{
public:
	void voice() override
	{
		cout << "miaow, miaow! ";
	}
};

class Duck : public Animal
{
public:
	void voice() override
	{
		cout << "bieak, bieak! ";
	}
};


int main()
{

	Dog dog = Dog();
	Cat cat = Cat();
	Duck duck = Duck();
	
	Animal* animals[] = {&dog, &cat, &duck};

	for (int i = 0; i < 3; i++)
	{
		animals[i]->voice();
	}
}


